
#include "stdafx.h"

#include "SerialStream.h"
#include <stdexcept>

SerialStream::SerialStream() {

	next_out = 0;
}

bool SerialStream::open(const char* commName) {

	hComm = CreateFileA(commName,                //port name
		GENERIC_READ | GENERIC_WRITE, //Read/Write
		0,                            // No Sharing
		NULL,                         // No Security
		OPEN_EXISTING,// Open existing port only
		0,            // Non Overlapped I/O
		NULL);        // Null for Comm Devices

	return (hComm != INVALID_HANDLE_VALUE);
}

bool SerialStream::baud(long baudRate) {

	DCB dcbSerialParams = { 0 }; // Initializing DCB structure
	dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
	if (GetCommState(hComm, &dcbSerialParams) == false) {
		return false;
	}

	dcbSerialParams.BaudRate = baudRate;  // Setting BaudRate = 9600
	dcbSerialParams.ByteSize = 8;         // Setting ByteSize = 8
	dcbSerialParams.StopBits = ONESTOPBIT;// Setting StopBits = 1
	dcbSerialParams.Parity = NOPARITY;  // Setting Parity = None

	if (SetCommState(hComm, &dcbSerialParams) == false) {
		return false;
	}

	return true;
}

bool SerialStream::available() { return false; }

uint8_t SerialStream::read() { return -1; }

bool SerialStream::flush() {

	int nextNotWritten = 0;
	while (nextNotWritten != next_out) {

		DWORD nWritten = 0;

		if (WriteFile(hComm,        // Handle to the Serial port
			out_buffer + nextNotWritten,     // Data to be written to the port
			next_out - nextNotWritten,  //No of bytes to write
			&nWritten, //Bytes written
			NULL) == false) {
			return false;
		}

		nextNotWritten = nextNotWritten + nWritten;
	}

	next_out = 0;
	return true;
}

bool SerialStream::write(uint8_t val) {
	bool result = true;

	if (next_out == BUFSIZE) {
		result = flush();
	}
	out_buffer[next_out] = val;
	++next_out;
	return result;
}

void SerialStream::close() {
	flush();
	CloseHandle(hComm);
}