// XBeeFFI.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <cstdint>

#include "SerialStream.h"
#include "XBee.h"

/*

To use this DLL:

1. Call XBeeOpenOn(commPort) where commPort is a string with the name of the serial
communication port that is connected to the Xbee transmitter.

2. Create a payload string with the contents of the message you wish to send.

3. Send commands using XBeeSendTo(destAddr, payload, payLoadLength)

destAddr		the address of the receiving XBee radio
payload			a pointer to the array that holds the message
payloadLength	the number of bytes in the message

4. When you no longer need to send messages call XBeeClose

*/
 
extern "C" __declspec(dllexport) bool XBeeOpenOn(char* commPort);
extern "C" __declspec(dllexport) void XBeeSendTo(uint16_t destAddr, uint8_t* payload, uint8_t payloadLength);
extern "C" __declspec(dllexport) void XBeeClose(void);

SerialStream serial;
XBee radio(serial);

bool XBeeOpenOn(char* commPort) {
	if (serial.open(commPort) == false)
		return false;
	if (serial.baud(115200) == false)
		return false;
	return true;
}

void XBeeSendTo(uint16_t destAddr, uint8_t* payload, uint8_t payloadLength) {
	Tx16Request request(destAddr, payload, payloadLength);
	radio.sendUnescaped(request);
}

void XBeeClose(void) {
	serial.close();
}