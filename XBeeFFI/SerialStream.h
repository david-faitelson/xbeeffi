#pragma once

#include <cstdint>
#include <windows.h>

#define BUFSIZE 64

class SerialStream {

private:
	uint8_t out_buffer[BUFSIZE];
	int next_out;

	HANDLE hComm;
public:

	SerialStream();

	bool open(const char*);

	bool baud(long);

	bool available();

	uint8_t read();

	bool flush();

	bool write(uint8_t val);

	void close();
};
